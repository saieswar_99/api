import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  gender: { type: String, required: true },
  email: { type: String, required: true },
  mobile: { type: Number, required: true },
});

export interface User extends mongoose.Document {
  id: string;
  name: string;
  gender: string;
  email: string;
  mobile: number;
}
