import {
    Controller,
    Post,
    Body,
    Get,
    Param,
    Patch,
    Delete,
  } from '@nestjs/common';
  import { UsersService } from './users.service';
  
  @Controller('users')
  export class UsersController {
    constructor(private readonly usersService: UsersService) {}
  
    @Post('create')
    async createOneUser(
      @Body('name') name: string,
      @Body('gender') gender: string,
      @Body('email') email: string,
      @Body('mobile') mobile: number,
    ) {
      const generatedId = await this.usersService.createOneUser(
        name,
        gender,
        email,
        mobile,
      );
      return { id: generatedId };
    }
  
    @Get()
    getAllUsers() {
      return this.usersService.getAllUsers();
    }
  
    @Get(':id')
    getOneUser(@Param('id') userId: string) {
      return this.usersService.getOneUser(userId);
    }
  
    @Patch(':id')
    updateUser(
      @Param('id') userId: string,
      @Body('name') userName: string,
      @Body('gender') userGender: string,
      @Body('email') userEmail: string,

      @Body('mobile') userMobile: number,
    ) {
      this.usersService.updateUser(userId, userName, userGender, userEmail, userMobile);
      return null;
    }
  
    @Delete(':id')
    deleteUser(@Param('id') userId: string) {
      this.usersService.deleteUser(userId);
      return null;
    }
  }
  