import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';

class ApiServiceMock {
  
  getUser(_firstName: string, _lastName: string) {
    return {
      name: 'Jane Doe',
      age: [20],
    };
  }
}

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getAge', () => {
    it('should get user Age', async () => {
      const expectedAge = 18;
      const age = UsersService.getAge('Jane', 'Doe');
      expect(age).toEqual(expectedAge);
    });
  });
});
