import { HttpService, Injectable } from '@nestjs/common';
import { User } from '../users/users.service';

@Injectable()
export class ApiService {
  constructor(private http: HttpService) {}
  async getUser(firstName: string, lastName: string): Promise<User> {
    const url = `../get-user?firstName=${firstName}&lastName=${lastName}`;
    const response = await this.http.get(url).toPromise();
    return response.data;
  }

  getHello(): string {
    return 'Hello World!';
  }
}