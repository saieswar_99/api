import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersService } from './users/users.service';

describe('AppController', () => {
  let appController: AppController;
  let spyService: UsersService;
  

  beforeEach(async () => {
    const ApiServiceProvider = {
      provide: UsersService,
      useFactory: () => ({
        getAGE: jest.fn(() => 20),
      }),
    }
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();
    

    appController = app.get<AppController>(AppController);
    spyService = app.get<UsersService>(UsersService);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });

  describe('getAGE', () => {
    it('should call getAGE for a user', async () => {
      const firstName = 'Joe';
      const secondName = 'Foo';
      appController.getUserAge(firstName, secondName);
      expect(spyService.getAge).toHaveBeenCalled();
    });
  });

  describe('getAGE', () => {
    it('should retrieve getAGE for a user', async () => {
      const firstName = 'Joe';
      const secondName = 'Foo';
      expect(spyService.getAge(firstName, secondName)).toBe(20);
    });
  });
});
